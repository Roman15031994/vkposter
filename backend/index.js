var express = require('express'),
	app = express(),
	path = require('path'),
	bodyParser = require('body-parser');

app.use(express.static(__dirname + '/app'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(8080);
